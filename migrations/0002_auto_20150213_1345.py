# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('parent', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payzatparent',
            name='is_first',
            field=models.BooleanField(default=False, verbose_name='Is Trial Balance Load'),
            preserve_default=True,
        ),
    ]
