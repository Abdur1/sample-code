# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PayzaatNotification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.TextField(verbose_name='body', blank=True)),
                ('is_read', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(related_name='user_notification', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PayzatParent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mobile_phone', models.CharField(max_length=30, null=True, verbose_name='Mobile Phone Number', blank=True)),
                ('balance', models.DecimalField(default=0.0, verbose_name='Available Balance', max_digits=10, decimal_places=3)),
                ('sms_notifications', models.PositiveSmallIntegerField(default=0, max_length=2, verbose_name='SMS Notifications', choices=[(0, 'Never'), (1, 'Low Balance')])),
                ('email_notifications', models.PositiveSmallIntegerField(default=0, max_length=2, verbose_name='Email Notifications', choices=[(0, 'Never'), (1, 'Low Balance'), (2, 'Every Transaction')])),
                ('subscription', models.PositiveSmallIntegerField(default=0, max_length=2, verbose_name='Subscription', choices=[(0, 'Trial Start'), (1, 'Trial End'), (2, 'Subscribed')])),
                ('is_first', models.BooleanField(default=False, verbose_name='First Balance Upload')),
                ('is_deleted', models.BooleanField(default=False, verbose_name='Deleted')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(related_name='payzat_parent', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ParentUser',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('accounts.payzatuser',),
        ),
    ]
