from django.template import Library, Node, TemplateSyntaxError

register = Library()

@register.simple_tag()
def multiply(qty, unit_price, *args, **kwargs):
    return qty * unit_price
