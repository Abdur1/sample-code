from django import forms
from django.db import transaction
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, ReadOnlyPasswordHashField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.conf import settings
from accounts.models import PayzatUser
from parent.models import PayzatParent
import logging
User = get_user_model()
logger = logging.getLogger(__name__)


class UserCreationForm(forms.ModelForm):
    MIN_LENGTH = settings.PASSWORD_MIN_LENGTH
    password1 = forms.CharField(
        label=_('Password'), widget=forms.PasswordInput(render_value=True))
    password2 = forms.CharField(
        label=_('Password confirmation'), widget=forms.PasswordInput(render_value=True))
    user_role = forms.ChoiceField(
        label=_("User Role"), choices=PayzatUser.USER_ROLES, initial=PayzatUser.PARENT)

    mobile_phone = forms.CharField(max_length=30, required=False)
    balance = forms.DecimalField(max_digits=10, decimal_places=3, initial=0.0)

    sms_notifications = forms.ChoiceField(choices=PayzatParent.SMS_SETTINGS, initial=PayzatParent.NEVER)
    email_notifications = forms.ChoiceField(choices=PayzatParent.EMAIL_SETTINGS, initial=PayzatParent.NEVER)
    subscription = forms.ChoiceField(choices=PayzatParent.SUBSCRIPTION_CHOICES, initial=PayzatParent.TRIAL_START)



    class Meta:
        model = User
        fields = (
            'email', 'first_name', 'last_name')

    def __init__(self, *args, **kwargs):

        super(UserCreationForm, self).__init__(*args, **kwargs)

    def clean_password1(self):
        password = self.cleaned_data.get('password1')

        # At least MIN_LENGTH long
        if len(password) < self.MIN_LENGTH:
            raise forms.ValidationError(
                "The password must be at least %d characters long." % self.MIN_LENGTH)

        # At least one letter and one non-letter
        first_isalpha = password[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password):
            raise forms.ValidationError("The password must contain at least one letter and at least one digit or"
                                        " punctuation character.")
        return password

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                _("Passwords don't match"))
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(
            commit=False)

        with transaction.atomic():
            try:
                parent = PayzatParent()
                parent.mobile_phone = self.cleaned_data['mobile_phone']
                parent.balance = self.cleaned_data['balance']
                parent.sms_notifications = self.cleaned_data['sms_notifications']
                parent.email_notifications = self.cleaned_data['email_notifications']
                parent.subscription = self.cleaned_data['subscription']

                user.set_password(self.cleaned_data["password1"])
                user.save()

                parent.user = user
                parent.save()

            except Exception as e:
                logger.error("Exception occurred while creating School",e)

        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label=_("Password"),
                                         help_text=_("Raw passwords are not stored, so there is no way to see "
                                                     "this user's password, but you can change the password "
                                                     "using <a href=\"%s\">this form</a>." % ('password')))

    mobile_phone = forms.CharField(max_length=30, required=False)
    balance = forms.DecimalField(max_digits=10, decimal_places=3, initial=0.0)

    sms_notifications = forms.ChoiceField(choices=PayzatParent.SMS_SETTINGS, initial=PayzatParent.NEVER)
    email_notifications = forms.ChoiceField(choices=PayzatParent.EMAIL_SETTINGS, initial=PayzatParent.NEVER)
    subscription = forms.ChoiceField(choices=PayzatParent.SUBSCRIPTION_CHOICES, initial=PayzatParent.TRIAL_START)

    # to verify parent's 1st balance upload
    is_first = forms.BooleanField(initial=False, required=False)
    is_deleted = forms.BooleanField(initial=False, required=False, label='Deleted')

    def __init__(self, *args, **kwargs):

        super(UserChangeForm, self).__init__(*args, **kwargs)

        if 'instance' in kwargs:
            user = kwargs['instance']

            self.initial['mobile_phone'] = user.payzat_parent.mobile_phone
            self.initial['balance'] = user.payzat_parent.balance
            self.initial['sms_notifications'] = user.payzat_parent.sms_notifications
            self.initial['email_notifications'] = user.payzat_parent.email_notifications
            self.initial['subscription'] = user.payzat_parent.subscription
            self.initial['is_first'] = user.payzat_parent.is_first
            self.initial['is_deleted'] = user.payzat_parent.is_deleted

    class Meta:
        model = User
        fields = (
            'email', 'password', 'user_role', 'first_name', 'last_name')

    def clean_password(self):
        return self.initial["password"]

    def save(self, commit=True):
        user = super(UserChangeForm, self).save(
            commit=False)

        with transaction.atomic():
            try:
                user.payzat_parent.mobile_phone = self.cleaned_data['mobile_phone']
                user.payzat_parent.balance = self.cleaned_data['balance']
                user.payzat_parent.sms_notifications = self.cleaned_data['sms_notifications']
                user.payzat_parent.email_notifications = self.cleaned_data['email_notifications']
                user.payzat_parent.subscription = self.cleaned_data['subscription']
                user.payzat_parent.is_first = self.cleaned_data['is_first']
                user.payzat_parent.is_deleted = self.cleaned_data['is_deleted']

                user.payzat_parent.save()
                user.save()

            except Exception as e:
                logger.error("Exception occurred while creating School",e)

        return user
