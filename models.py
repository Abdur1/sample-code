from django.db import models
from django.db.models import signals
from django.db.models import Q
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from nexmomessage import NexmoMessage
from django.conf import settings

from accounts.models import PayzatUser
from transactions.models import PayzatTransaction
from school.models import SchoolSubscription

from payzat.tasks import create_student_proxy_server


class DeletedParentManager(models.Manager):
    def get_queryset(self):
        return super(DeletedParentManager, self).get_queryset().filter(is_deleted=False)


class ParentManager(models.Manager):
    def get_queryset(self):
        return super(ParentManager, self).get_queryset()


class PayzatParent(models.Model):
    user = models.OneToOneField(
        PayzatUser, related_name='payzat_parent')
    mobile_phone = models.CharField(
        verbose_name=_("Mobile Phone Number"), max_length=30, null=True, blank=True)
    balance = models.DecimalField(verbose_name=_("Available Balance"),
                                  max_digits=10, decimal_places=3, default=0.0)

    NEVER = 0
    LOW_BALANCE = 1
    SMS_SETTINGS = (
        (NEVER, _('Never')),
        (LOW_BALANCE, _('Low Balance'))
    )

    sms_notifications = models.PositiveSmallIntegerField(verbose_name=_(
        "SMS Notifications"), choices=SMS_SETTINGS, default=0, blank=False, null=False, max_length=2)

    LOW_BALANCE = 1
    EVERY_TRANSACTION = 2
    EMAIL_SETTINGS = (
        (NEVER, _('Never')),
        (LOW_BALANCE, _('Low Balance')),
        (EVERY_TRANSACTION, _('Every Transaction')),
    )

    email_notifications = models.PositiveSmallIntegerField(verbose_name=_(
        "Email Notifications"), choices=EMAIL_SETTINGS, default=0, blank=False, null=False, max_length=2)

    TRIAL_START = 0
    TRIAL_END = 1
    SUBSCRIBED = 2
    SUBSCRIPTION_CHOICES = (
        (TRIAL_START, _('Trial Start')),
        (TRIAL_END, _('Trial End')),
        (SUBSCRIBED, _('Subscribed')),
    )

    subscription = models.PositiveSmallIntegerField(verbose_name=_(
        "Subscription"), choices=SUBSCRIPTION_CHOICES, default=0, blank=False, null=False, max_length=2)

    # to verify parent's 1st balance upload
    is_first = models.BooleanField(
        default=False, verbose_name=_("Is Trial Balance Load"))

    is_deleted = models.BooleanField(
        default=False, verbose_name=_("Deleted"))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    get_all = ParentManager()
    objects = DeletedParentManager()

    def __unicode__(self):
            return '%s %s %s' % (self.user.first_name, self.user.middle_name,
                                          self.user.last_name)


class ParentUser(PayzatUser):

    class Meta:
        proxy = True


class PayzaatNotification(models.Model):
    user = models.ForeignKey(
        PayzatUser, related_name='user_notification')
    message = models.TextField(_("body"), blank=True)
    is_read = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


def send_notification(sender, instance, created, **kwargs):
    message = render_to_string('parent/low_balance.txt')
    if isinstance(instance, PayzatParent) and  not created:
        if instance.sms_notifications == PayzatParent.LOW_BALANCE:
            if instance.mobile_phone is not None and instance.balance < 3:
                settings.NEXMO_MSG['to'] = instance.mobile_phone
                settings.NEXMO_MSG['text'] = message
                sms = NexmoMessage(settings.NEXMO_MSG)
                sms.set_text_info(settings.NEXMO_MSG['text'])
                response = sms.send_request()
    if isinstance(instance, PayzatTransaction) and created:
        if instance.to_person and instance.to_person.user_role == PayzatUser.PARENT:
            parent = instance.to_person.payzat_parent
        elif instance.from_person and instance.from_person.user_role == PayzatUser.PARENT:
            parent = instance.from_person.payzat_parent
        else:
            return
        if parent.email_notifications == PayzatParent.LOW_BALANCE:
            if parent.balance < 3:
                notification = PayzaatNotification()
                notification.user, notification.message, notification.is_read = parent.user, message, False
                notification.save()
                send_mail("Payzaat Balance Alert", message,
                          settings.DEFAULT_FROM_EMAIL, [parent.user.email])
        elif parent.email_notifications == PayzatParent.EVERY_TRANSACTION:
            notification = PayzaatNotification()
            notification.user, notification.message, notification.is_read = parent.user, instance.description, False
            notification.save()
            send_mail("Payzaat Transaction Detail", instance.description,
                      settings.DEFAULT_FROM_EMAIL, [parent.user.email])


signals.post_save.connect(send_notification, sender=PayzatTransaction)
signals.post_save.connect(send_notification, sender=PayzatParent)
