from django.conf.urls import patterns, url
from parent.views import ParentHome, ParentSignUpView, StudentRegistrationPhaseOne, StudentRegistrationPhaseTwo, \
    ParentProfile, ParentStudentProfiles, ParentStudentProfileView, parent_change_password, \
    deposit_amount, refund_amount, ParentProfile, ParentStudentProfiles, ParentStudentProfileView,\
    parent_change_password, EnableDisableStudentDevice,   DeleteStudentDevice, TransactionView,\
    set_daily_limit, NotificationSettings, ListNotifications, DeleteNotification, update_notification,\
    BuySubscription, ParentFunding


urlpatterns = patterns('',
                       url(r'^signup/$', ParentSignUpView.as_view(),
                           name='parent_signup'),
                       url(r'home/$', ParentHome.as_view(),
                           name='parent_home'),
                       url(r'transactions/$', TransactionView.as_view(),
                           name='transactions'),
                       url(r'transactions/(?P<from_date>\w+)/$', TransactionView.as_view(),
                           name='transactions'),
                       url(r'transactions/(?P<from_date>\w+)/(?P<to_date>\w+)/$', TransactionView.as_view(),
                           name='transactions'),
                       url(r'add/student/$', StudentRegistrationPhaseOne.as_view(),
                           name='student_register'),
                       url(r'add/student/profile/$', StudentRegistrationPhaseTwo.as_view(),
                           name='student_register_add_profile'),
                       url(r'student/profile/(?P<id>\d+)/$', StudentRegistrationPhaseTwo.as_view(),
                           name='student_edit_profile'),
                       url(r'profile/$', ParentProfile.as_view(),
                           name='parent_profile'),
                       url(r'profile/change-password/$', parent_change_password,
                           name='parent_profile_change_password'),
                       url(r'student/profiles/$', ParentStudentProfiles.as_view(),
                           name='parent_students_profiles'),
                       url(r'student/view/(?P<id>\d+)/$',
                           ParentStudentProfileView.as_view(), name='parent_student_view'),
                       url(r'deposit/$', deposit_amount,
                           name='parent_deposit'),
                       url(r'refund/$', refund_amount,
                           name='parent_refund'),
                       url(r'daily_limit/$', set_daily_limit,
                           name='daily_limit'),
                       url(r'device/disable/(?P<device_id>\d+)/(?P<student_id>\d+)/$',
                           EnableDisableStudentDevice.as_view(), name='enable_disable_student_device'),
                       url(r'device/delete/(?P<device_id>\d+)/(?P<student_id>\d+)/$',
                           DeleteStudentDevice.as_view(), name='delete_student_device'),

                       url(r'settings/$', NotificationSettings.as_view(),
                           name='parent_settings'),

                       url(r'notifications/$', ListNotifications.as_view(),
                           name='notifications_list'),

                       url(r'notification/delete/(?P<notification_id>\d+)/$',
                            DeleteNotification.as_view(), name='delete_notification'),

                       url(r'notification/update/$',
                            update_notification, name='update_notification'),

                       url(r'subscription/buy/$',
                            BuySubscription.as_view(), name='buy_subscription'),

                       url(r'funding/$',
                            ParentFunding.as_view(), name='parent_funding'),

                       )
