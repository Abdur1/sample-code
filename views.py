import string
import decimal
import json
import random
from django.utils import timezone

from django.db import transaction
from django.db.models import Q
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.views.generic import TemplateView, FormView, DeleteView, UpdateView, ListView
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
User = get_user_model()

from account.views import SignupView
from accounts.forms import SignUpForm
from accounts.models import PayzatAccount
from parent.forms import StudentRegistrationFormOne, StudentRegistrationFormTwo,\
    ParentProfileForm, ParentSettingsForm,\
    ChangeParentPassword, DepositAmountForm, ParentTransactionForm, AddFundsForm
from parent.models import PayzatParent, PayzaatNotification
from student.models import PayzatStudent
from student.forms import DailyLimitForm
from accounts.decorators import not_login_required, parent_required,\
    not_expired_parent_required
from device.models import NFCTag, PayzatDevices
from transactions.models import PayzatTransaction
from subscription.models import StudentSubscription

class ParentHome(TemplateView):
    model = PayzatTransaction
    template_name = 'parent/home.html'

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ParentHome, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(
            ParentHome, self).get_context_data(**kwargs)

        student_users = []
        for student in PayzatStudent.objects.filter(parent=self.request.user.payzat_parent):
            student_users.append(student.user)

        transactions = PayzatTransaction.objects.filter(Q(from_person=self.request.user)
                                                        | Q(to_person=self.request.user)
                                                        | Q(from_person__in=student_users)
                                                        | Q(to_person__in=student_users)).\
            select_related("from_terminal").\
            select_related("to_terminal").\
            select_related("from_user").select_related("to_user").\
            select_related("terminal_location").order_by('-created')[:5]

        context['transactions'] = transactions
        return context


class ParentProfile(UpdateView):
    template_name = 'parent/parent-profile.html'
    form_class = ParentProfileForm
    model = PayzatParent

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ParentProfile, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        parent = form.save()
        messages.success(
            self.request, 'Profile successfully updated.')
        return HttpResponseRedirect(reverse('parent:parent_home'))

    def get_object(self, queryset=None):
        obj = self.request.user.payzat_parent
        return obj


@parent_required
def parent_change_password(request):
    if request.method == 'POST':
        form = ChangeParentPassword(request, request.POST)
        if form.is_valid():
            user = authenticate(
                email=request.user.email,
                password=form.cleaned_data.get('current_password'))
            if user:
                user.set_password(
                    form.cleaned_data.get('password'))
                user.save()
                messages.success(
                    request, _('Password successfully updated.'))
                return HttpResponseRedirect(reverse('parent:parent_home'))
        return render(request, 'parent/parent-profile.html', {'form': form})
    else:
        return HttpResponseRedirect(reverse('parent:parent_home'))


def transaction_do(request, parent, student, amount):
    with transaction.atomic():
        transaction_log = PayzatTransaction()

        transaction_log.from_person = request.user
        transaction_log.to_person = student.user
        transaction_log.amount = amount
        transaction_log.type = PayzatTransaction.WEB
        transaction_log.transaction_time = timezone.now()
        transaction_log.description = "Parent %s deposited %.3f KWD amount to %s" % (
            request.user, amount, student)
        transaction_log.save()

        parent.balance -= amount
        if parent.subscription == PayzatParent.TRIAL_START and parent.balance == 0:
            parent.subscription = PayzatParent.TRIAL_END
        parent.save()

        student.balance += amount
        if student.subscription == PayzatStudent.TRIAL_START:
            student.subscription = PayzatStudent.TRIAL_END
        student.save()
        messages.success(
            request, _('Successfully deposited %.3f KWD amount to %s.' % (amount, student)))

@parent_required
def deposit_amount(request):
    if request.method == 'POST':
        form = DepositAmountForm(request, request.POST)
        if form.is_valid():
            student_id = form.cleaned_data.get('student_id')
            amount = form.cleaned_data.get('amount', None)
            parent = request.user.payzat_parent
            try:
                student = PayzatStudent.objects.get(
                    user__pk=student_id, parent=request.user.payzat_parent)
                parent_balance = request.user.payzat_parent.balance
            except PayzatStudent.DoesNotExist as e:
                messages.error(
                    request, 'You are not authorized to perform this action.')
                return HttpResponseRedirect(reverse('parent:parent_student_view', kwargs={'id': student_id}))

            if parent_balance >= amount:
                if parent.subscription == PayzatParent.TRIAL_START:
                    transaction_do(request, parent, student, amount)
                elif parent.subscription == PayzatParent.SUBSCRIBED:
                    if student.subscription == PayzatStudent.SUBSCRIBED:
                        transaction_do(request, parent, student, amount)
                    else:
                        messages.warning(
                            request, _("Your student's subscription expired.Please buy subscription."))
                return HttpResponseRedirect(reverse('parent:parent_student_view', kwargs={'id': student_id}))
            else:
                messages.error(request, _('insufficient amount.'))
        messages.error(request, form.errors)
        return HttpResponseRedirect(reverse('parent:parent_student_view',
                                            kwargs={'id': form.cleaned_data.get('student_id')}))
    else:
        return HttpResponseRedirect(reverse('parent:parent_home'))


@parent_required
def refund_amount(request):
    if request.method == 'POST':
        form = DepositAmountForm(request, request.POST)
        if form.is_valid():
            student_id = form.cleaned_data.get('student_id')
            amount = form.cleaned_data.get('amount', None)
            parent = request.user.payzat_parent
            try:
                student = PayzatStudent.objects.get(
                    user__pk=student_id, parent=request.user.payzat_parent)
                student_balance = student.balance
            except PayzatStudent.DoesNotExist as e:
                messages.error(
                    request, 'You are not authorized to perform this action.')
                return HttpResponseRedirect(reverse('parent:parent_student_view', kwargs={'id': student_id}))

            if student_balance >= amount:
                with transaction.atomic():
                    transaction_log = PayzatTransaction()

                    transaction_log.from_person = student.user
                    transaction_log.to_person = request.user
                    transaction_log.amount = amount
                    transaction_log.type = PayzatTransaction.WEB
                    transaction_log.transaction_time = timezone.now()
                    transaction_log.description = "Parent %s refunded %.3f KWD amount from %s" % (
                        request.user, amount, student)
                    transaction_log.save()

                    parent.balance += amount
                    parent.save()

                    student.balance -= amount
                    student.save()
                messages.success(
                    request, _('Successfully refunded %.3f KWD amount from %s.' % (amount, student)))
                return HttpResponseRedirect(reverse('parent:parent_student_view', kwargs={'id': student_id}))
            else:
                messages.error(request, _('insufficient amount.'))
        messages.error(request, form.errors)
        return HttpResponseRedirect(reverse('parent:parent_student_view',
                                            kwargs={'id': form.cleaned_data.get('student_id')}))
    else:
        return HttpResponseRedirect(reverse('parent:parent_home'))


@parent_required
def set_daily_limit(request):
    if request.method == 'POST':
        form = DailyLimitForm(request, request.POST)
        if form.is_valid():
            student_id = form.cleaned_data.get('student_id')
            amount = form.cleaned_data.get('amount', None)
            status = form.cleaned_data.get('status', None)
            try:
                student = PayzatStudent.objects.get(
                    user__pk=student_id, parent=request.user.payzat_parent)

                student.daily_limit = amount
                student.limit_status = status
                student.save()
                messages.success(
                    request, 'Student daily spent limit successfully updated in the system.')
            except PayzatStudent.DoesNotExist as e:
                messages.error(
                    request, 'You are not authorized to perform this action.')
            return HttpResponseRedirect(reverse('parent:parent_student_view', kwargs={'id': student_id}))
        messages.error(request, form.errors)
        return HttpResponseRedirect(reverse('parent:parent_student_view',
                                            kwargs={'id': form.cleaned_data.get('student_id')}))
    else:
        return HttpResponseRedirect(reverse('parent:parent_home'))


class ParentSignUpView(SignupView):
    form_class = SignUpForm
    identifier_field = 'email'

    @method_decorator(not_login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ParentSignUpView, self).dispatch(request, *args,
                                                      **kwargs)

    def create_user(self, form, commit=True, **kwargs):
        user = super(ParentSignUpView, self).create_user(
            form=form, commit=False,  **kwargs)
        user.first_name = form.cleaned_data.get(
            "first_name")
        user.last_name = form.cleaned_data.get("last_name")
        user.is_active = False

        user.user_role = User.PARENT
        if commit:
            user.save()

        return user

    def after_signup(self, form):
        mobile_phone = form.cleaned_data.get(
            "mobile_phone", None)
        payzat_parent = PayzatParent(
            mobile_phone="+965%s" % mobile_phone, user=self.created_user)
        payzat_parent.save()

        messages.success(self.request,
                         'Thanks for registering in Payzat. '
                         'Confirmation email is been sent to your '
                         'account. Kindly check your inbox.')
        super(ParentSignUpView, self).after_signup(form)

    def generate_username(self, form):
        # django-user-accounts requires us to generate dummy username
        # unfortunately.
        username = form.cleaned_data.get('email', ''.join(random.SystemRandom().choice(string.uppercase +
                                                                                       string.digits) for _ in xrange(15)))
        return username


class StudentRegistrationPhaseOne(FormView):
    template_name = 'parent/student-registration-phase-one.html'
    form_class = StudentRegistrationFormOne

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(StudentRegistrationPhaseOne, self).dispatch(request,
                                                                 *args, **kwargs)

    def form_valid(self, form):
        self.request.session['std_reg_code'] = form.cleaned_data[
            'registration_code']
        return HttpResponseRedirect(reverse
                                    ('parent:student_register_add_profile'))


class ParentStudentProfiles(TemplateView):
    template_name = 'parent/parent-student-profiles.html'

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ParentStudentProfiles, self).dispatch(request,
                                                           *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(
            ParentStudentProfiles, self).get_context_data(**kwargs)
        students = PayzatStudent.objects.filter(
            parent=self.request.user.payzat_parent)
        context['students'] = students
        return context


class ParentStudentProfileView(FormView):
    template_name = 'parent/student-view.html'

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ParentStudentProfileView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if context.get('profile', None):
            return self.render_to_response(context)
        else:
            messages.error(self.request, 'Invalid Student Id.')
            return HttpResponseRedirect(reverse('landing_page'))

    def get_context_data(self, **kwargs):
        context = super(
            ParentStudentProfileView, self).get_context_data(**kwargs)
        user_id = kwargs.get('id', None)
        if user_id:
            try:
                logged_in_parent = self.request.user.payzat_parent
                student = PayzatStudent.objects.get(
                    parent=logged_in_parent, user__pk=user_id)
                devices = list(
                    student.student_devices.all())
                context['profile'] = student
                context['devices'] = devices
                context['parent'] = logged_in_parent
            except PayzatStudent.DoesNotExist as e:
                messages.error(self.request, 'Invalid Student Id.')
        context['deposit_form'] = DepositAmountForm
        context['daily_limit_form'] = DailyLimitForm
        return context


class StudentRegistrationPhaseTwo(FormView):
    template_name = 'parent/student-registration-phase-two.html'
    form_class = StudentRegistrationFormTwo

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(StudentRegistrationPhaseTwo, self).dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super(
            StudentRegistrationPhaseTwo, self).get_initial()

        student_registration_code = self.request.session.get(
            'std_reg_code', None)
        if student_registration_code:
            try:
                nfc_tag = NFCTag.objects.get(
                    nfc_code=student_registration_code)
                nfc_device = nfc_tag.nfc_device
                if nfc_device:
                    initial["school"] = nfc_device.school
                    initial[
                        "reg_code"] = student_registration_code
                else:
                    messages.error(
                        self.request, 'Invalid NFC code.')

            except NFCTag.DoesNotExist:
                messages.error(
                    self.request, 'Invalid NFC code.')
        else:
            messages.error(
                self.request, 'Kindly enter registration code of student.')

        return initial

    def get_form_kwargs(self):
        kwargs = super(
            StudentRegistrationPhaseTwo, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        student = form.save()
        messages.success(
            self.request, 'Student successfully added in the system.')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('parent:parent_students_profiles')


class EnableDisableStudentDevice(TemplateView):

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EnableDisableStudentDevice, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if kwargs.get('device_id', None) and kwargs.get('student_id', None):
            logged_in_parent = self.request.user.payzat_parent
            user_id = kwargs.get('student_id', None)
            device_id = kwargs.get('device_id', None)
            try:
                student = PayzatStudent.objects.get(
                    parent=logged_in_parent, pk=user_id)
                device = PayzatDevices.objects.get(
                    pk=device_id, student=student)
                if device.nfc_tag.nfc_tag_status == NFCTag.ACTIVE:
                    device.nfc_tag.nfc_tag_status = NFCTag.DISABLED
                    device.nfc_tag.save()
                    messages.success(
                        self.request, 'Device Disabled Successfully.')
                elif device.nfc_tag.nfc_tag_status == NFCTag.DISABLED:
                    for student_device in NFCTag.objects.filter(nfc_device__student=student):
                        student_device.nfc_tag_status = NFCTag.DISABLED
                        student_device.save()
                    device.nfc_tag.nfc_tag_status = NFCTag.ACTIVE
                    device.nfc_tag.save()
                    messages.success(
                        self.request, 'Device Enabled Successfully.')
            except PayzatStudent.DoesNotExist, PayzatDevices.DoesNotExist:
                messages.error(
                    self.request, 'You are not authorized to perform this action.')
        else:
            messages.error(
                self.request, 'Invalid Device Id or student id.')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('device:list_device')


class DeleteStudentDevice(DeleteView):
    template_name = 'device/delete_device.html'
    model = PayzatDevices

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteStudentDevice, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        obj = PayzatDevices.objects.get(
            pk=self.kwargs.get('device_id', None))
        return obj

    def delete(self, request, *args, **kwargs):
        if kwargs.get('device_id', None) and kwargs.get('student_id', None):
            logged_in_parent = self.request.user.payzat_parent
            user_id = kwargs.get('student_id', None)
            device_id = kwargs.get('device_id', None)
            try:
                student = PayzatStudent.objects.get(
                    parent=logged_in_parent, pk=user_id)
                device = PayzatDevices.objects.get(
                    pk=device_id, student=student)
                if device.nfc_tag.nfc_tag_status == NFCTag.DISABLED:
                    device.nfc_tag.nfc_tag_status = NFCTag.DELETED
                    device.nfc_tag.save()
                    messages.success(
                        self.request, 'Device Deleted Successfully.')
                else:
                    messages.error(
                        self.request, 'You can only delete a disable device.')
            except PayzatStudent.DoesNotExist, PayzatDevices.DoesNotExist:
                messages.error(
                    self.request, 'You are not authorized to perform this action.')
        else:
            messages.error(
                self.request, 'Invalid Device Id or student id.')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('device:list_device')


class TransactionView(TemplateView):
    model = PayzatTransaction
    form_class = ParentTransactionForm
    template_name = 'parent/transactions.html'

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(TransactionView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(
            TransactionView, self).get_context_data(**kwargs)

        student_users = []
        for student in PayzatStudent.objects.filter(parent=self.request.user.payzat_parent):
            student_users.append(student.user)

        transactions = PayzatTransaction.objects.filter(Q(from_person=self.request.user)
                                                        | Q(to_person=self.request.user)
                                                        | Q(from_person__in=student_users)
                                                        | Q(to_person__in=student_users)).\
            select_related("from_terminal").\
            select_related("to_terminal").\
            select_related("from_user").select_related("to_user").\
            select_related("terminal_location")
        if self.request.GET.get("from_date"):
            from_date = self.request.GET.get("from_date")
            transactions = transactions.filter(transaction_time__gt=from_date)

        if self.request.GET.get("to_date"):
            to_date = self.request.GET.get("to_date")
            transactions = transactions.filter(transaction_time__lt=to_date)

        context['transactions'] = transactions
        return context


class NotificationSettings(SuccessMessageMixin, UpdateView):
    template_name = 'parent/notification_settings.html'
    form_class = ParentSettingsForm
    success_message = "Parent Notification Settings updated successfully."

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(NotificationSettings, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        obj = PayzatParent.objects.get(user=self.request.user)
        return obj

    def get_success_url(self):
        return reverse('parent:parent_settings')


class ListNotifications(ListView):
    model = PayzaatNotification
    template_name = 'parent/notifications_list.html'

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListNotifications, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(ListNotifications, self).get_queryset().filter(user=self.request.user)


class DeleteNotification(SuccessMessageMixin, DeleteView):
    template_name = 'parent/delete_notification.html'
    model = PayzatDevices
    success_message = "Notification Deleted successfully."

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteNotification, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        try:
            obj = PayzaatNotification.objects.get(user=self.request.user,
                pk=self.kwargs.get('notification_id', None))
        except PayzaatNotification.DoesNotExist:
            obj = PayzaatNotification()
        return obj

    def delete(self, request, *args, **kwargs):
        if kwargs.get('notification_id', None):
            try:
                notification = PayzaatNotification.objects.get(user=self.request.user,
                pk=self.kwargs.get('notification_id', None))
                notification.delete()
                messages.success(
                    self.request, 'Notification Deleted Successfully.')
            except PayzaatNotification.DoesNotExist as e:
                messages.error(
                    self.request, 'You are not authorized to perform this action.')
        else:
            messages.error(
                self.request, 'Invalid Notification Id.')
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('parent:notifications_list')


@parent_required
def update_notification(request):
    for record in PayzaatNotification.objects.filter(user=request.user, is_read=False):
        record.is_read = True
        record.save()

    return HttpResponse(json.dumps({'success': True })
                            ,content_type='application/javascript; charset=utf8')


class ParentFunding(FormView):
    template_name = 'parent/parent_funding.html'
    form_class = AddFundsForm

    @method_decorator(not_expired_parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ParentFunding, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        parent = form.save()
        messages.success(
            self.request, 'Funds successfully added to your account.')
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super(ParentFunding, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_success_url(self):
        return reverse('parent:parent_home')



class BuySubscription(TemplateView):
    template_name = 'parent/subscriptions.html'

    @method_decorator(parent_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BuySubscription, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(
            BuySubscription, self).get_context_data(**kwargs)

        context['students'] = PayzatStudent.objects.filter(
            ~Q(subscription=PayzatStudent.SUBSCRIBED),
            parent=self.request.user.payzat_parent
        )
        return context

    def post(self, request, *args, **kwargs):
        student_ids = request.POST.getlist('student')
        parent = request.user.payzat_parent
        total_amount = 0
        insert_list = []
        try:
            with transaction.atomic():
                for student in PayzatStudent.objects.filter(parent=self.request.user.payzat_parent, pk__in=student_ids):
                    insert_list.append(StudentSubscription(
                        parent=parent,
                        student=student,
                        status=StudentSubscription.PENDING,
                        subscription=student.school.subscription,
                    ))
                    total_amount += student.school.subscription.subscription_fee
                    student.subscription = PayzatStudent.SUBSCRIBED
                    student.subscription_date=timezone.now()
                    student.save()
                if len(insert_list) >=2:
                    total_amount -= (total_amount * 20)/100
                StudentSubscription.objects.bulk_create(insert_list)

                transaction_log = PayzatTransaction()

                transaction_log.from_person = self.request.user
                transaction_log.is_payzaat = True
                transaction_log.amount = total_amount
                transaction_log.type = PayzatTransaction.WEB
                transaction_log.transaction_time = timezone.now()
                msg = "%s buy subscription  in %.3f KWD amount" % \
                    (self.request.user, total_amount)
                transaction_log.description = msg
                transaction_log.save()

                parent.subscription = PayzatParent.SUBSCRIBED
                parent.save()

                try:
                    payzat = PayzatAccount.objects.get()
                    payzat.balance += decimal.Decimal(total_amount)
                    payzat.save()
                except PayzatAccount.DoesNotExist:
                    pass
                #redirect to knet
                # from e24PaymentPipe import Gateway
                #
                # pgw = Gateway('resource.cgn', 'alias')
                # pgw.error_url = 'http://localhost:8000/parent/subscription/buy/'
                # pgw.response_url = 'http://localhost:8000/parent/subscription/buy/'
                # pgw.amount = 1.0
                # gw_info = pgw.get_payment_url()

                messages.success(
                self.request, _("subscribed successfully"))
        except Exception as e:
            messages.error(
                self.request, _("Some error occurred. try again"))
        return HttpResponseRedirect(reverse('parent:buy_subscription'))
