from __future__ import unicode_literals

from parent.models import PayzaatNotification


def notification(request):
    """Provide the count of unread notifications for an authenticated user."""
    if request.user.is_authenticated():
        notifications = PayzaatNotification.objects.filter(user=request.user, is_read=False).\
            order_by('-created')[:5]
        return {'payzaat_notification_count': len(notifications),
                'notifications': notifications}
    else:
        return {}
