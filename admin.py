from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.contrib.auth.admin import UserAdmin
from django.conf import settings
from parent.models import ParentUser
from parent.admin_forms import UserCreationForm, UserChangeForm
from accounts.models import PayzatUser
User = get_user_model()


class ParentAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = (
        'email', 'first_name', 'middle_name', 'last_name', 'user_role')
    list_filter = ()
    fieldsets = (
        ('Parent User', {
         'fields': ('email', 'first_name', 'middle_name', 'last_name', 'password')}),
        ('User Type', {'fields': ('user_role',)}),
        ('Permissions', {'fields': ('is_active',)}),
        ('Payzaat Parent', {'fields': ('mobile_phone', 'balance', 'sms_notifications', 'email_notifications',
                                        'subscription', 'is_first', 'is_deleted')}),
    )
    add_fieldsets = (
        ('Parent User', {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'middle_name', 'last_name', 'password1', 'password2', 'is_active',
                       'user_role')}
         ),
        ('Payzaat Parent', {
            'classes': ('wide',),
            'fields': ('mobile_phone', 'balance', 'sms_notifications', 'email_notifications',
                                        'subscription')}),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()
    actions = ['delete_model']

    def get_queryset(self, request):
        return super(ParentAdmin, self).get_queryset(request).filter(user_role=PayzatUser.PARENT,
                                                                     payzat_parent__is_deleted=False)

    def get_actions(self, request):
        actions = super(ParentAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def delete_model(self, request, object):
        if isinstance(object, PayzatUser):
            self.delete_parent(object)
        else:
            for obj in object:
                if isinstance(obj, PayzatUser):
                    self.delete_parent(obj)
    delete_model.short_description = "Delete selected"

    def delete_parent(self, obj):
        obj.payzat_parent.is_deleted = True
        obj.email += '%s%s' % (settings.DELETE_STRING_SEPARATOR, timezone.now())
        obj.save()
        obj.payzat_parent.save()


admin.site.register(ParentUser, ParentAdmin)
