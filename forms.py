from django.utils import timezone
from django import forms
from django.db import transaction
from django.conf import settings
from django.forms.models import ModelForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from django.core.validators import MaxValueValidator
User = get_user_model()

from account.forms import SignupForm
from student.models import PayzatStudent
from device.models import NFCTag
from parent.models import PayzatParent
from transactions.models import PayzatTransaction


class SignUpForm(SignupForm):
    MIN_LENGTH = settings.PASSWORD_MIN_LENGTH
    first_name = forms.CharField(
        max_length=30, label=_('First Name'))
    last_name = forms.CharField(
        max_length=30, label=_('Last Name'))
    mobile_phone = forms.CharField(
        max_length=30, label=_('Mobile Phone'))

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        del self.fields["username"]

    def clean_password(self):
        password = self.cleaned_data.get('password')

        # At least MIN_LENGTH long
        if len(password) < self.MIN_LENGTH:
            raise forms.ValidationError(
                "The password must be at least %d characters long." % self.MIN_LENGTH)

        # At least one letter and one non-letter
        first_isalpha = password[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password):
            raise forms.ValidationError("The password must contain at least one letter and at least one digit or"
                                        " punctuation character.")
        return password


class StudentRegistrationFormOne(forms.Form):
    registration_code = forms.CharField(required=True, label=_("Registration Code"))

    def clean_registration_code(self):
        student_registration_code = self.cleaned_data.get(
            'registration_code')
        if student_registration_code:
            try:
                nfc_tag = NFCTag.objects.get(
                    nfc_code=student_registration_code, nfc_tag_status=NFCTag.USED)
                nfc_device = nfc_tag.nfc_device
                if nfc_device:
                    if nfc_device.student:
                        raise forms.ValidationError(
                            _("Registration code is already assigned to another student."))
                else:
                    raise forms.ValidationError(
                        _("Invalid Registration code."))

            except NFCTag.DoesNotExist:
                raise forms.ValidationError(
                    _("Invalid Registration code."))
        else:
            raise forms.ValidationError(
                _("Registration code is required."))

        return student_registration_code


class StudentRegistrationFormTwo(ModelForm):
    first_name = forms.CharField(
        label=_('First Name'), required=True, max_length=50)
    middle_name = forms.CharField(
        label=_('Middle Name'), required=False, max_length=50)
    last_name = forms.CharField(
        label=_('Last Name'), required=True, max_length=50)
    email = forms.EmailField(
        label=_("Email"), required=True, max_length=255)
    reg_code = forms.CharField(required=True, label=_("Registration Code"))
    password = forms.CharField(
        widget=forms.PasswordInput(), label=_("Password"), min_length=6)
    password_confirm = forms.CharField(
        widget=forms.PasswordInput(), label=_("Confirm Password"), min_length=6)

    class Meta:
        model = PayzatStudent
        exclude = ['user', 'registration_code', 'parent','subscription']

    def __init__(self, *args, **kwargs):
        if kwargs['request']:
            self.request = kwargs['request']
            del kwargs['request']
        super(StudentRegistrationFormTwo, self).__init__(
            *args, **kwargs)
        self.fields['reg_code'].widget.attrs[
            'readonly'] = True
        self.fields['school'].widget.attrs[
            'disabled'] = 'disabled'

    def clean_reg_code(self):
        submitted_registration_code = self.cleaned_data.get(
            'reg_code', None)
        actual_registration_code = self.request.session.get(
            'std_reg_code', None)
        if submitted_registration_code and actual_registration_code and submitted_registration_code \
                != actual_registration_code:
            raise forms.ValidationError(
                _("Invalid registration code."))

        return submitted_registration_code

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("This email is already in use.")
        return email

    def clean_school(self):
        student_registration_code = self.request.session.get(
            'std_reg_code', None)
        try:
            nfc_tag = NFCTag.objects.get(
                nfc_code=student_registration_code)
            nfc_device = nfc_tag.nfc_device
            if nfc_device:
                submitted_school = nfc_device.school
                if submitted_school != self.cleaned_data['school']:
                    raise forms.ValidationError(
                        _("Invalid school."))
        except NFCTag.DoesNotExist:
            raise forms.ValidationError(
                _("Invalid school."))
        return self.cleaned_data['school']

    def clean_password_confirm(self):
        password_confirm = self.cleaned_data.get(
            'password_confirm')
        password = self.cleaned_data.get('password')
        if password and password != password_confirm:
            raise forms.ValidationError(
                'Password do not match')
        return password_confirm

    def save(self, commit=True):
        student = super(StudentRegistrationFormTwo, self).save(
            commit=False)

        first_name = self.cleaned_data.get(
            'first_name', None)
        middle_name = self.cleaned_data.get(
            'middle_name', None)
        last_name = self.cleaned_data.get('last_name', None)
        email = self.cleaned_data.get('email')
        user = User(first_name=first_name, middle_name=middle_name,
                    last_name=last_name, email=email)
        password = self.cleaned_data.get('password')
        user.set_password(password)
        user.save()

        profile_image = self.cleaned_data['profile_image']
        birth_day = self.cleaned_data.get('birthday', None)
        gender = self.cleaned_data.get('gender', None)
        grade = self.cleaned_data.get('grade', None)
        registration_code = self.cleaned_data['reg_code']
        school = self.cleaned_data.get('school')

        if profile_image:
            student.profile_image = profile_image
        student.birth_day = birth_day
        student.gender = gender
        student.grade = grade
        student.user = user
        student.registration_code = registration_code
        student.school = school
        student.parent = self.request.user.payzat_parent
        student.save()

        nfc_tag = NFCTag.objects.get(
            nfc_code=registration_code)
        nfc_device = nfc_tag.nfc_device
        if nfc_device:
            nfc_device.student = student
            nfc_tag.nfc_tag_status = NFCTag.ACTIVE
            nfc_tag.save()
            nfc_device.save()
        del self.request.session['std_reg_code']
        return student


class ParentProfileForm(ModelForm):
    first_name = forms.CharField(
        label=_('First Name'), required=True, max_length=50)
    middle_name = forms.CharField(
        label=_('Middle Name'), required=False, max_length=50)
    last_name = forms.CharField(
        label=_('Last Name'), required=True, max_length=50)
    email = forms.EmailField(
        label=_("Email"), required=True, max_length=255)

    class Meta:
        model = PayzatParent
        fields = ['first_name', 'middle_name',
                  'last_name', 'email', 'mobile_phone']

    def save(self, commit=True):
        parent = super(ParentProfileForm, self).save(
            commit=False)
        first_name = self.cleaned_data.get(
            'first_name', None)
        middle_name = self.cleaned_data.get(
            'middle_name', None)
        last_name = self.cleaned_data.get('last_name', None)
        email = self.cleaned_data.get('email')
        mobile_phone = self.cleaned_data.get(
            'mobile_phone', None)

        parent.user.first_name = first_name
        parent.user.middle_name = middle_name
        parent.user.last_name = last_name
        parent.user.email = email
        parent.user.save()
        parent.mobile_phone = mobile_phone
        parent.save()

        return parent


class ChangeParentPassword(forms.Form):
    current_password = forms.CharField(widget=forms.PasswordInput(), label=_("Current Password"), required=True,
                                       min_length=6)
    password = forms.CharField(widget=forms.PasswordInput(), label=_(
        "New Password"), min_length=6, required=True)
    password_confirm = forms.CharField(widget=forms.PasswordInput(), label=_("Confirm Password"), required=True,
                                       min_length=6)

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(ChangeParentPassword, self).__init__(
            *args, **kwargs)

    def clean_current_password(self):
        user = authenticate(email=self.request.user.email,
                            password=self.cleaned_data.get('current_password'))
        if user:
            return self.cleaned_data.get('current_password')
        else:
            raise forms.ValidationError(
                _('Invalid Current Password.'))

    def clean_password(self):
        password = self.cleaned_data.get('password')
        # At least one letter and one non-letter
        first_isalpha = password[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password):
            raise forms.ValidationError("The password must contain at least one letter and at least one digit or"
                                        " punctuation character.")
        return password

    def clean_password_confirm(self):
        password_confirm = self.cleaned_data.get(
            'password_confirm', None)
        password = self.cleaned_data.get('password', None)
        if password and password != password_confirm:
            raise forms.ValidationError(
                'Password and Confirm Password do not match')
        return password_confirm


class DepositAmountForm(forms.Form):
    amount = forms.DecimalField(required=True, min_value=0.001)
    student_id = forms.IntegerField(required=True)

    def __init__(self, request=None, *args, **kwargs):
        if request:
            self.request = request
        super(DepositAmountForm, self).__init__(*args, **kwargs)

    def clean_student(self):
        student = self.cleaned_data.get('student', None)
        try:
            student = PayzatStudent.objects.get(pk=student.pk,
                                                parent=self.request.user.payzat_parent)
        except PayzatStudent.DoesNotExist as e:
            raise forms.ValidationError('Invalid student.')
        return student


class ParentTransactionForm(forms.Form):
    from_date = forms.DateField(label=_("From Date"), required=True)
    to_date = forms.DateField(label=_("To Date"), required=True)

    def __init__(self, request=True, *args, **kwargs):
        if request:
            self.request = request
        super(ParentTransactionForm, self).__init__(*args, **kwargs)

    def clean(self):
        super(ParentTransactionForm, self).clean()

        cleaned_data = self.cleaned_data
        from_date = self.cleaned_data.get('from_date', None)
        to_date = self.cleaned_data.get('to_date', None)

        if to_date < from_date:
            raise forms.ValidationError(
                'To date should be after from date.')

        return cleaned_data


class ParentSettingsForm(ModelForm):

    class Meta:
        model = PayzatParent
        fields = ['sms_notifications', 'email_notifications', ]


class AddFundsForm(forms.Form):

    balance = forms.DecimalField(required=True, min_value=0.001)

    def __init__(self, request=True, *args, **kwargs):
        if request:
            self.request = request
        super(AddFundsForm, self).__init__(*args, **kwargs)
        validators = [ v for v in self.fields['balance'].validators if not isinstance(v, MaxValueValidator) ]
        validators.append( MaxValueValidator(self.request.user.payzat_parent.parent_students.count() * 5) )
        self.fields['balance'].validators = validators

    def save(self, commit=False):
        balance = self.cleaned_data.get('balance', None)
        parent = self.request.user.payzat_parent
        with transaction.atomic():
            transaction_log = PayzatTransaction()

            transaction_log.to_person = self.request.user
            transaction_log.amount = balance
            transaction_log.type = PayzatTransaction.WEB
            transaction_log.transaction_time = timezone.now()
            transaction_log.description = "%s funded %.3f KWD amount to account." % (
                self.request.user, balance)
            transaction_log.save()

            parent.balance += balance
            parent.is_first = True
            parent.save()

            transaction_log.save()
